#include <iostream>

/**
 * Entry point of application.
 * 
 * @param int     argc Number of command line arguments passed.
 * @param char[]* argv List of argument strings.
 * 
 * @return Application completion code.
 */
int main (int argc, char * argv[]) {
	std::cout << "hello";
	
	return (0);
}
